# HTMLinXR plugin

A plugin for visualizing in 3D/XR the real-time DOM structure of the opened HTML file.

## Features

A 3D visualization of the DOM offers a more intuitive and comprehensive understanding of the hierarchical structure and relationships within HTML documents. Additionally, a 3D perspective can enhance cognitive load management, allowing developers to navigate complex structures with greater ease and efficiency.

We rely on [BabiaXR](https://babiaxr.gitlab.io) for the visualization, it relies as well on [A-Frame](https://aframe.io) which is a 3D engine built on top of the [three.js](https://threejs.org/) library.

Executing the command `HTML 3D/XR viewer` you can visualize the structure of DOM of the HTML opened in the IDE in 3D, using the mouse and the keyboard you can navigate the scene. Also works in XR using a XR device.

Run `Cmd+Shift+P` and execute the `HTML 3D/XR viewer` command to open the viewer.

![Example](https://i.imgur.com/7T0s0fd.gif)

## Known Issues

- 

## Release Notes

### 1.0.5

- Solve bug with binaries, scene.html file was not rigth included

### 1.0.4

- Save body content in a JSON to setup AR environment in real time.

### 1.0.3

- Dependencies fixed

### 1.0.2

- Buttons to collapse/expand the 3D DOM
- Buttons to rotate visualization in the three axis

### 1.0.1

- HTML rendered on children as a canvas texture
- Separation of parent/child increased, added a yellow line to see relationship

### 1.0.0

Initial release of the plugin, with the following features:

1. Run `Cmd+Shift+P` and execute the `HTML 3D/XR viewer` command to open the viewer.
2. Each visible item has its own box in the scene.
3. Each box has a different color.

