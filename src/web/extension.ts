// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as path from 'path';


// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "html-in-xr" is now active in the web extension host!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('html-in-xr.html3dxrViewer', () => {
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		// vscode.window.showInformationMessage('Hello World from HTML in XR in a web extension host!');

		// Get the active editor
		const editor = vscode.window.activeTextEditor;
		if (editor) {

			// Check the language id
			if (editor.document.languageId === 'html') {

				// Create and show panel
				const panel = vscode.window.createWebviewPanel(
					'html3dxrViewer', // Identificador único
					'HTML 3D/XR viewer', // Título que se muestra en la parte superior
					vscode.ViewColumn.One, // Columna en la que se mostrará (puedes cambiar esto)
					{
						enableScripts: true // Habilita la ejecución de scripts en la vista web
					}
				);

				// Get the document content  
				const doc = editor.document;
				const html = doc.getText();
				let htmlContentTemplate: any;

				// Get the local path to main script run in the webview, then convert it to a uri we can use in the webview.
				const sceneHtmlUri = vscode.Uri.joinPath(context.extensionUri, 'dist', 'web', 'scene.html');
				vscode.workspace.fs.readFile(sceneHtmlUri).then(bytes => {

					// Load scene template html file
					htmlContentTemplate = new TextDecoder().decode(bytes);

					// Extact only the content in <head>, since is what we are going to put in the head of our scene in order to load other js dependencies
					const headContent: string = extractTagContent(html, 'head');
					const updatedHtmlContent: string = insertIntoHead(htmlContentTemplate, headContent);
					// Extact only the content in <body>, since is what we are going to visualize in 3D
					const bodyContent: string = extractTagContent(html, 'body').replace(/"/g, '&quot;').replace(/'/g, '&#39;');
					const finalHtmlContent: string = replaceBabiaHtml(updatedHtmlContent, bodyContent);

					// Save the initial content
					saveContentToFile(JSON.stringify({ html: bodyContent }), 'htmlinxr_data.json', doc.uri);

					// Render the scene
					panel.webview.html = finalHtmlContent; //+ bodyContent;



				});

				// Register save listener for updating the file
				let i = 1
				vscode.workspace.onDidSaveTextDocument(document => {
					if (document === vscode.window.activeTextEditor?.document) {
						// Check if the document is html
						if (vscode.window.activeTextEditor?.document.languageId === 'html') {
							// Get new updated text
							const updatedHTML = vscode.window.activeTextEditor?.document.getText();
							// Update scene with the updated html
							// Extact only the content in <head>, since is what we are going to put in the head of our scene in order to load other js dependencies
							const headContent: string = extractTagContent(updatedHTML, 'head');
							const updatedHtmlContent: string = insertIntoHead(htmlContentTemplate, headContent);
							// Extact only the content in <body>, since is what we are going to visualize in 3D
							const bodyContent: string = extractTagContent(updatedHTML, 'body').replace(/"/g, '&quot;').replace(/'/g, '&#39;');
							const finalHtmlContent: string = replaceBabiaHtml(updatedHtmlContent, bodyContent);

							// Render the scene
							//i++
							//panel.webview.html = String(i)
							panel.webview.html = finalHtmlContent;

							// Save the initial content
							saveContentToFile(JSON.stringify({ html: bodyContent }), 'htmlinxr_data.json', doc.uri);
						} else {
							vscode.window.showErrorMessage('HTML 3D/XR viewer -- Active editor must contain HTML');
						}
						// If not HTML, don't do anything else
					}
				});

			} else {

				// Show error message  
				vscode.window.showErrorMessage('HTML 3D/XR viewer -- Active editor must contain HTML');

			}
		}
	});


	context.subscriptions.push(disposable);
}

function extractTagContent(htmlString: string, tagName: string): string {
	// Crear las etiquetas de inicio y cierre basadas en el tagName
	const startTag = `<${tagName}>`;
	const endTag = `</${tagName}>`;

	// Buscar las posiciones de las etiquetas de inicio y cierre
	const tagStart: number = htmlString.indexOf(startTag);
	const tagEnd: number = htmlString.indexOf(endTag);

	// Si ambas etiquetas existen, extraer el contenido entre ellas
	if (tagStart !== -1 && tagEnd !== -1) {
		return htmlString.substring(tagStart + startTag.length, tagEnd);
	} else {
		// Si no hay etiquetas, devolver el string original
		return htmlString;
	}
}

function extractSpecificTags(htmlString: string, tags: string[]): string {
	let extractedContent = '';

	tags.forEach(tag => {
		let regex;
		if (tag === 'link') {
			regex = new RegExp(`<${tag}[^>]*>`, 'gi');  // Self-closing tags
		} else {
			regex = new RegExp(`<${tag}[^>]*>[\\s\\S]*?<\\/${tag}>`, 'gi');  // Regular tags
		}
		const matches = htmlString.match(regex);
		if (matches) {
			extractedContent += matches.join('\n') + '\n';
		}
	});

	return extractedContent;
}

function insertIntoHead(htmlContent: string, headContent: string): string {
	const headStartTag = '<head>';
	const headEndTag = '</head>';

	const headStartIndex: number = htmlContent.indexOf(headStartTag);
	const headEndIndex: number = htmlContent.indexOf(headEndTag);

	if (headStartIndex !== -1 && headEndIndex !== -1) {
		const headTagsContent = extractSpecificTags(headContent, ['style', 'script', 'link']);
		const beforeHead = htmlContent.substring(0, headEndIndex);
		const afterHead = htmlContent.substring(headEndIndex);
		return `${beforeHead}${headTagsContent}${afterHead}`;
	} else {
		return htmlContent;
	}
}

function replaceBabiaHtml(updatedHtmlContent: string, bodyContent: string): string {
	// Crear un patrón para encontrar la línea con el argumento babia-html
	const babiaHtmlPattern = /babia-html="html: <h1>BabiaXRHTMLtarget<\/h1>/;
	// Reemplazar el contenido de <h1>BabiaXRHTMLtarget</h1> con el contenido de bodyContent
	const replacement = `babia-html="html: ${String(bodyContent)}`;

	return updatedHtmlContent.replace(babiaHtmlPattern, replacement);
}

function saveContentToFile(content: string, fileName: string, activeFileUri: vscode.Uri) {
	const folderUri = activeFileUri.with({ path: activeFileUri.path.replace(/\/[^\/]+$/, '') });
	const fileUri = folderUri.with({ path: `${folderUri.path}/${fileName}` });

	const writeData = new TextEncoder().encode(content);
	vscode.workspace.fs.writeFile(fileUri, writeData).then(() => {
		//vscode.window.showInformationMessage(`Saved content to ${fileUri.fsPath}`);
		console.log(`Saved content to ${fileUri.fsPath}`);
	});
}

// This method is called when your extension is deactivated
export function deactivate() { }
